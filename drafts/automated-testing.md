# Automated Testing

Writing code that functions correctly can be difficult. It's even more challenging when we have to ensure that code continues to work correctly as functionality and requirements change. Automated tests help to confirm that our code works correctly and will continue to do so as the code base changes.

Once we have an automated test suite, we can run this test suite whenever a change could cause behavior to change.

- good code design leads to good test design

## Types of Tests

- dependencies; pieces outside of the subject of a test should be assume to be working

As usual, tests should follow separation of concerns. Each test should have something specific that is trying to test and should be written in a way that isolates that concern as much as possible.

A test still might have dependencies on other code, such as on any code that's needed for setup. Any code that isn't the focus of the test should be assumed to be working correctly.

- should strategies for dealing with non-correct code be placed here?

### Dependencies

It can sometimes be hard to get code into a testable state. This is often because of dependencies on other code, behaviors, or systems. This comes up in a variety of ways in the code we're trying to test:

- Dependencies on other classes or functions.

  The code we're testing often doesn't work in isolation. It usually relies on other code. This can make it difficult to set up or configure our code to be in a testable state.

- Using I/O operations.

  I/O operations are a part of pretty much all systems. We need to do things like fetch data from external sources or to persist data in a database. Testing functionality that depends on I/O operations can be difficult because we need to be able to reliable reproduce the behavior we want. It's also hard because there might be actual behaviors or properties that we want to avoid for the sake of testing, such as our web request returning an error because that web server is having problems.

  Trying to produce desired behavior often means doing one of two things. You could make actual calls to the system you integrate with. This would be representative of the actual code behavior. However, this would also mean using the production system in your tests---it will need test data in it and any issues with the system will propagate into your tests. As alluded to, you won't be able to run your tests if the integrated system is down or behaving incorrectly.

  Alternatively, you could make calls to an imitation system that's just used for testing. That allows for better control over what that system is doing and how it behaves for your test. The downside here is that you need an entirely separate system. You need to ensure that your imitation system actually matches all of the desired behaviors of the real system.

There are two related strategies for handling dependencies. First, remove or abstract away the dependency. Normally, removing the dependency isn't an often. Abstracting away dependencies allows us to change our dependencies so that unrelated systems are no longer tightly coupled.

As an example of abstracting a dependency, suppose your code needs a database connection. It clearly needs some data that you keep in a database. But our code doesn't care about the database itself---just the resulting data that we fetch from it. We can first factor out all our database calls into a separate class. This new class takes information about the data we want, performs the relevant calls, then parses and returns the results. Our original code no longer depends on the database connection.

We're not done, however. Our code doesn't directly depend on a database connection, but it now it depends on this new class that does depend on a database connection. So we have to abstract again. Our new class is a database repository---it fetches and stores data, backed by a database implementation. What we care about is the fetching and storing of data, not the implementation. So, we can factor out a repository interface for this class. This interface looks quite similar to the new class we made: it takes information about the data we want and promises to return the desired results. It just doesn't say anything about how it will do so.

At this point, our code depends on this repository interface for fetching some data. We also have a database implementation of this interface, which we will use in our production code. However, we've completley removed our database dependency! Our original code doesn't know or care whether or not a database is being used.

This takes us to our second strategy: creating test implementations for our dependencies. 

### Unit Tests

- TODO: better definition

A unit test covers a "unit" of code. These units typically atomic, behavioral units.

As described by their name, unit tests are the smallest type of test. They cover a small, low-level "unit" of your code base. There is no exact definiton for the scope of a unit. It will depend on the code you're testing and personal preferences. I prefer to have the test suite cover a class, with each test covering a single method.

In addition to being small, there are other important qualities of unit tests. They need to be fast, reliable, and help localize problems.

In addition to being small, unit tests must also be very fast. One, this enables developers to frequently test the code they are working on without eating up their time. Two, a sizable code base can easily have thousands of unit tests. If each one took a second to run, running all of the tests could easily take the better portion of an hour.

Unit tests should also be reliable. They'll be run the most often and there will be more unit tests than other types of tests. Because of this, flaky unit tests will significantly slow down development.

#### Test Doubles

Very often, the unit being tested depends on other functions or classes. Because they aren't the focus of our unit test, they should be assumed to be working correctly.

However, even when working correctly, we might not want to depend on these other objects. That's because they might violate the properties we want for our unit tests (namely, by being slow or unreliable). Or, they might just be awkward or unwieldy to use.

We can get around these issues with test doubles. A test double is an object used in place of the actual object we depend on. For example, we can substitute a SQL database with an in-memory one.

Test doubles allow us to trade off properties that are important in production for properties that are important for our tests. This way we will have correct, fast, and reliable unit tests even when our production code doesn't have all of these properties.

test double patterns


https://martinfowler.com/bliki/UnitTest.html

### Integration Tests

### Acceptance Tests

## Test Maintenance

- what to do with a failing test
  - a failing test does not mean that the code is worng--the test may be wrong