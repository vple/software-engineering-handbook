# Creating New Branches

Create a new branch:

```git
git branch branch-name
```

Create and checkout a new branch:

```git
git checkout -b branch-name
```

Create and checkout a new branch that tracks a remote branch:

```git
git checkout -b branch-name --track remote-branch
```

# Interrupted Workflow

As you're doing work, you might get interrupted and need to switch to another branch to do something. Odds are you'll have made some changes and git won't let you switch branches until you commit or stash them.

Generally, it's pretty easy to `git stash` your changes. Later, you can come back to them with `git stash pop`. However, there is a slight downside to this: you have to remember what you have stashed and which branch those changes belong to.

Here's an alternate approach: just make a commit! You can use a placeholder commit message, such as:

> WIP: Description of your change.

When you come back to your branch and are ready to resume work, you can get run `git reset --soft HEAD~`. This undoes your commit, leaving the changed files in staging. If you don't want them there, you can follow that up with `git unstage`.

# Pulling a Branch Without Checkouts
Typically, when you pull changes from a remote branch onto a local branch, you run

```git
git checkout <branch>
git pull --rebase
```

The problem with this is if that branch is very different from your current branch, such as if it's many commits behind. Git has to recreate that branch state, even though you're about to pull to get up to date.

If your're doing a fast-forward merge, you can instead fetch changes from the remote without needing to checkout that branch.

```git
git fetch <remote> <source-branch>:<destination-branch>
```

In the event you want to merge a local branch into another branch, you can instead run

```git
git fetch . <source-branch>:<destination-branch>
```

If you're doing a non-fast-forward merge, you can add instead use:

```git
git fetch <remote> +<source-branch>:<destination-branch>
```

# Pulling With a Dirty Repo
Sometimes, you'll want to fetch the latest changes for your branch but already have existing changes that haven't been added to a commit. Git won't let you pull because of these existing changes. It gives a message like this:

```bash
$ git pull --rebase
error: cannot pull with rebase: You have unstaged changes.
error: please commit or stash them.
```

As suggested, if your changes aren't ready to be committed then you can stash them.

```
git stash
```

This puts your changes onto a stack, separate from your working repo. You can now pull in the latest changes. Afterwards, you can get your changes back by popping them off the stack.

```
git stash pop
```