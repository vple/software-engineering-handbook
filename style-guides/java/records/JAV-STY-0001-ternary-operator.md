# JAV-STY-0001: Ternary Operator

Vincent Le  
Accepted: 2018-10-06

## Context
There are many potential ways to wrap the ternary operator, `?:`, across multiple lines.

There are two multi-line styles given by the Sun Java Code Convetions and the Gwydion style guide:

```
(condition) ? trueValue
            : falseValue
```

```
(condition)
    ? trueValue
    : falseValue
```

The Google style guide specifies [breaking before non-assignment operators](http://google.github.io/styleguide/javaguide.html#s4.5.1-line-wrapping-where-to-break).

The OpenJDK guide recommends including [redundant parentheses](http://cr.openjdk.java.net/~alundblad/styleguide/index-v6.html#toc-redundant-parentheses) only when the precedence or associativity of the expression is less clear without the parentheses. The guide also implicitly follows the second style above.[^openjdk-wrapping]

[^openjdk-wrapping]: [http://cr.openjdk.java.net/~alundblad/styleguide/index-v6.html#toc-wrapping-lines](http://cr.openjdk.java.net/~alundblad/styleguide/index-v6.html#toc-wrapping-lines)

## Decision
Multi-line ternaries should be written as follows:

```
condition
    ? trueValue
    : falseValue
```

This is similar to the second style above, but doesn't require parentheses. Parentheses may be used if they improve readability, as described by the OpenJDK guide. However, this usually won't be necessary—the condition will already be on its own line. It should also be easy to realize that this is a ternary based on the following lines.

The first style above is rejected because it's unwieldy. If the condition is long, the alignment will be awkward. This is also likely to be the case, given that the ternary needs to be line-wrapped in the first place.

## Resources

* [Sun Java Code Conventions](https://www.oracle.com/technetwork/java/codeconventions-150003.pdf)
* [Gwydion Style Guide](http://www.cs.cmu.edu/~gwydion/Sheets/doc/style-guide-for-java.html)
* [Google Java Style Guide](http://google.github.io/styleguide/javaguide.html)
* [OpenJDK Java Style Guidelines](http://cr.openjdk.java.net/~alundblad/styleguide/index-v6.html)