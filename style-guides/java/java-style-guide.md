# Java Style Guide

Java style guidelines for writing clean code.

There are places where I use the style defined in another guide. In these places, I'll link to the corresponding guide.

## Syntax
### Ternary Operator
The ternary operator is the `?:` operator.

#### Inline Style

```java
condition ? trueValue : falseValue
```

#### Multi-line Style[^JAV-STY-0001]

[^JAV-STY-0001]: [JAV-STY-0001: Ternary Operator](records/JAV-STY-0001-ternary-operator.md)

The `?` and `:` symbols come after the line break, starting the next line. These lines are indented one extra level. This makes it clear where each case starts.

```java
condition
    ? trueValue
    : falseValue
```

### Switch Statements

#### Terminology

Inside of a *switch block* are one or more *statement groups*. Each statement group consists of one or more *switch labels* (`case FOO:` or `default:`), followed by one or more statements.[^google-java-switch]

[^google-java-switch]: <https://google.github.io/styleguide/javaguide.html#s4.8.4-switch>

```java
switch (expression) {
    case 1:
        // handle case
        break;
    case 2:
    case 3:
        // handle cases
        break;
    default:
        // handle default
        break;
}
```

In this switch block, there are three statement groups. The second statement group has two switch labels.

#### Statement Groups
Each statement group should terminate the switch block. This is often done with `break`, `return`, or `throws`.

##### Fall-throughs
A fall-through is where execution in one statement group continues to the next statement group.

Fall-throughs are strongly discouraged and should be avoided. This is mainly because it's easy for developers to misread and write code that unintentionally affects multiple statement groups.

If you must use a fall-through, you should clearly indicate that a statement group might fall through to the next one.

```java
switch (expression) {
    case 1:
        // case 1 logic
        // fall through
    case 2:
        // case 2 logic
        break;
    default:
        break;
}
```

#### Default Case
Every switch block should include a default case, even if it contains no code. This makes it explicit what the default behavior is.

Empty default cases should still include a `break` statement.

> **Exception**  
> Switch statements for enum types may exclude the default case only if they have explicit cases to cover all possible enum values. This allows IDEs and static analysis tools to warn when cases are missed.

# Additional Resources

https://google.github.io/styleguide/javaguide.html

https://www.oracle.com/technetwork/java/codeconventions-150003.pdf

http://www.cs.cmu.edu/~gwydion/Sheets/doc/style-guide-for-java.html