# Basic Workflow
The simplest way to use git is to make your changes directly on your master branch, without worrying about other branches. That workflow looks like this:

1. Get the most recent changes.  
2. Develop your feature.
3. Stage your changes.
4. Make your commit.
5. Get the most recent changes, again.
6. Push your changes.

## Get The Most Recent Changes
### Start With A Clean Branch
To start, you want to make sure that you're working from a clean master branch.

To check what branch you're on, run `git branch`. This will show you the branches you have in your repository, as well as which branch is your current branch. For this workflow, you want the `master` branch to be selected. Your output will look something like this:

```bash
$ git branch
* master
```

To check what changes are currently on your branch, run `git status`. This will show any file changes you have that haven't been committed. If your repo is clean, meaning that you have no changes, this will look like this:

```bash
$ git status
On branch master
Your branch is up to date with 'origin/master'.

nothing to commit, working tree clean
```

Strictly speaking, you don't have to have a clean repo in order to develop stuff. It just helps because there's less to worry about and less that can go wrong. For our workflows, we'll assume that we always start with a clean branch.

### Pull Latest Changes
At this point, there's nothing new on our local branch. However, there may be new changes on our origin repo. We want to pull in those changes so that we're working from the latest version of the code base. We can do that with:

```
git pull --rebase
```

This command fetches all changes from the `master` branch on the remote origin repo. That branch is referred to as `origin/master`. It then rebases those changes onto your current branch. This means that the changes from `origin/master` are added onto your `master` branch. If you had any local changes, they are reapplied on top of these new changes.

## Develop Your Feature
This one's up to you--make the changes you need!

## Stage Your Changes
Once you're happy with your changes, you need to actually get them into a commit. First, a look at the state of your repo with `git status`:

```bash
$ git status
On branch master
Your branch is up to date with 'origin/master'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   drafts/git.md

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	drafts/abstraction.md
	drafts/git-workflows.md
	drafts/patterns.md

no changes added to commit (use "git add" and/or "git commit -a")
```

Any modified files that are already being tracked in your repository will be listed in the "Changes not staged for commit" section. Any new files that git isn't tracking are listed in the "Untracked files" section. Files listed in either of these sections are currently **not** going to be a part of your commit.

In order to add changes to a commit, they first have to be **staged**. To stage a file, run

```
git add <file>
```

For example, I can run `git add drafts/git-workflows.md` to stage that file. Running `git status` again, I can now see that file is going to be committed:

```bash
$ git status
On branch master
Your branch is up to date with 'origin/master'.

Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

	new file:   drafts/git-workflows.md

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   drafts/git.md

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	drafts/abstraction.md
	drafts/patterns.md
```

You should add all of the files that you want to include in your commit. To add all files, you can run `git add --all`.

## Make Your Commit
Once you have staged files, you can add them to a commit:

```
git commit
```

As part of creating your commit, you will be asked to write a commit message. This is a description of the changes that are being included in your commit.

## Get The Most Recent Changes
Once you have your commit, you're just about ready to push it back to your origin repo. However, someone else may have pushed some changes while you were developing. In that case, git won't let you push your changes because your repo isn't up to date.

To handle this, we just need to make sure we have the latest changes. Run `git pull --rebase` again to ensure that your repo is up to date.

## Push Your Changes
To finish, push your changes:

```
git push
```


# Single Local Branch Workflow
The basic workflow will usually work for a lot of changes that you need to make. However, it's best suited for making a single change at a time. When you need to work on multiple unrelated changes simultaneously, it can be a pain to manage all of these changes on a single branch.

Using local branches makes it much easier to handle this more complicated workflow. Additionally, learning to use branches will allow you to do much more with git. To start, we'll look at a workflow that uses a single local development branch.

## Create A Branch
To start, make sure you're working from a clean `master` branch. We can then create a new branch.

```
git branch <branch-name>
```

We're going to name our branch `development`. We now have two branches:

```bash
$ git branch development
$ git branch
  development
* master
```

Note that we're still on our `master` branch. To switch to our new `development` branch, we use `git checkout`:

```
git checkout development
```

## Make Your Changes
Now that we're on our new branch, we can make our changes as before. We develop our changes, stage them, and then create a commit.

## Pushing Your Changes
Now that we have our changes, we want to push them. As before, we need to make sure that we have the latest changes before we can actually push. There are multiple ways this can be done.

### Directly To `origin/master`
With this method, we'll sync our `development` branch directly with the remote `origin/master` branch. First, we run

```
git fetch origin
```

This updates our local repo with the changes from our remote origin, but doesn't actually update any of our local branches. To get those new changes onto our `development` branch, we run

```
git rebase origin/master
```

This rebases those new changes onto our current branch, which should still be the `development` branch. Finally, we push those changes:

```
git push origin <local-branch>:<remote-branch>
```

In this case, we want to run `git push origin development:master`. This pushes our changes to our remote origin, putting the changes from `development` onto `origin/master`.

### Via `master`
In this alternate approach, we first move our changes onto our `master` branch. We need to make sure that our `development` branch has the latest changes from `master`. To do this, we run

```
git rebase master
```

This copies changes from `master` onto our current `development` branch, applying any commits we had on top. We now want to move all of our new commits onto the `master` branch.

```
git checkout master
git rebase development
```

Now we're on `master` and have the changes we want to push. From here, we proceed as with our basic workflow:

```
git pull --rebase
git push
```

# Other Local Branch Workflows
There isn't much of a difference between our basic workflow and our single local branch workflow. However, understanding how to use local branches opens up a lot of other workflows.


## Multiple Local Branches
There's no reason that we're limited to using a single `development` branch. We can make as many local branches as we need. In particular, we can make a branch for each feature we need to develop. This allows us to work on all of these features independently.

## Branching Branches
Usually, we create our new branches as offshoots of our `master` branch. Sometimes, we'll need to develop something more complicated, with multiple features. In this case, it can be useful to branch off of another branch.

Let's say we're building out a shopping cart for an online candy store. We start by making a branch for that:

```
git checkout master
git checkout -b shopping-cart
```

We're using `git checkout -b <branch-name>` here, which creates and checks out a branch in one command. It's the same thing as a `git branch <branch-name>` followed by a `git checkout <branch-name>`.

We can then make and commit some changes to build out our shopping cart. Eventually, we get to a point where we need to support two different types of candy purchases. Some candies are sold on a per quantity basis. Others are sold on a per weight basis. We're going to make a branch for each of these, branching from our `shopping-cart` branch.

```
git branch quantity
git branch weight
```

We can now separately build out our per quantity and per weight purchase types. Once we're done, we can rebase them onto our main `shopping-cart` branch. This is done similarly to the "Via `master`" step from before. Eventually, once we've finished our shopping cart product, everything on that branch can be added to our `master` branch.	

## Tracking Remote Branches
With our local branch workflow, it takes several steps to get our branches up to date when we're ready to push. This is kind of annoying. Fortunately, we can simplify this by having our local branches track a remote branch.

When we have our local branch track a remote branch, we are telling it which remote branch it should sync to. As we'll see, we'll be able to use a workflow more similar to our basic workflow when working on that branch.

### Tracking A Remote Branch
When you're creating a new branch, you can configure it to track a branch. This is done with

```
git checkout -b <branch-name> --track <remote-branch>
```

Bear in mind that the remote branch's name includes the name of the remote repo. Right now, we'll be using `origin/master` as our remote branch.

If you're already on an existing branch, you can configure it with

```
git branch -u <remote-branch>
```

### Syncing With the Remote Branch
Now that we're tracking a remote branch, we can directly pull changes from that branch. This is done with

```
git pull --rebase
```

To push to the remote branch, we use

```
git push
```

These are exactly the same as what we did with our basic workflow! That's not by accident--the `master` branch tracks `origin/master` by default.

### Things To Look Out For
By default, git only allows you to push to remote branches with the same name as your local branch. This is mainly a safety measure. We can change the setting to allow pushing to any remote branch, regardless of its name. This is done with

```
git config --global push.default upstream
```

This changes our default push settings to allow pushing to our upstream branch.

With this configuration, be extra aware of which branch you're pushing to. I recommend using `git branch -vv` instead of just `git branch` when taking a look at your branch status. These arguments tell git to display some additional info, including the remote branches that are being tracked.

Finally, changes on our local branch can still be moved around our repo as needed. We can still use commands like `git rebase` in case we need to move changes from / onto other local branches.

# Using Remote Branches
With local branches, we're able to develop multiple features on the same machine. What about developing a feature across multiple machines? You might, for example, want to coordinate your work across multiple computers. Or, you might need to coordinate development with someone else. We'll assume that two developers, Alice and Bob, want to jointly develop a new feature.

We can handle this by using remote branches. A remote branch is simply a branch on a remote repository. And a remote repository is a repository on another machine. From Alice's perspective, both `origin` and Bob's repo are remote repositories.

## Adding Remotes
If we're not using `origin`, we'll want to add the remote repository. You can see what remote repositories you have configured with `git remote -v`:

```bash
$ git remote -v
origin	https://vple@bitbucket.org/vple/software-engineering-handbook.git (fetch)
origin	https://vple@bitbucket.org/vple/software-engineering-handbook.git (push)
```

To add a new remote, use

```
git remote add <name> <url>
```

You can use other protocols in your url, such as SSH. Additionally, you'll need to be able to access the repo you specify.

## Creating A Remote Branch
Once our repo knows about the remote repo that we want to use, we can create a local branch. This is the same as before:

```
git checkout -b development
```

To create the remote branch, we use

```
git push <remote-name> <remote-branch-name>
```

So, for Alice to create this remote branch on origin, she would run `git push origin development`. The name of the remote branch doesn't have to be the same as your local branch, but keeping the two the same makes the relationship more clear.

As described above, we'll often want our local branch to track the remote branch. We can set this when we create the remote branch:

```
git push -u <remote-name> <remote-branch-name>
```

Or, we can set this after creating the remote branch:

```
git branch -u <remote-branch>
```

In this second command, remember that the remote branch includes the remote's name. In our case, `origin/development`.

## Checking Out A Remote Branch
Bob can now check out and track the remote branch that Alice has created.

```
git checkout --track origin/development
```

## Using Remote Branches
At this point, using the remote branch is straightforward. We can treat it like any of our other flows. The only difference is that we use our local `development` branch instead of `master` and that it pushes to `origin/development` instead of `origin/master`.
